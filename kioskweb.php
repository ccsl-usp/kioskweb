<?php
// Authors: Leonardo Schaffer, Nelson Lago,
// and João Guilherme Barbosa de Souza
// Distributed under the GPLv3 license

require_once("DASPRiD/Enum/autoload.php");
require_once("bacon/BaconQrCode/autoload.php");

use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use BaconQrCode\Renderer\Color\Rgb;
use BaconQrCode\Renderer\RendererStyle\Fill;

require 'tmhOAuth.php';

ini_set('max_execution_time', 300);

$IMAGES_DIR = "images";

$PAGES_DIR = "pages";

$VIDEOS_DIR = "videos";

function escape_quotes($text) {
    return str_replace('"', '\"', $text);
}

function resize_image($image, $newimage, $width, $height) {
    global $IMAGES_DIR;

    if (!file_exists("images_resized/$newimage")) {
        $image = new imagick("$IMAGES_DIR/$image");

        $image->resizeImage($width, $height,
                            imagick::FILTER_LANCZOS, 1.0, false);

        $image->writeImage("images_resized/$newimage");
    }
}

function images() {
    global $IMAGES_DIR;

    if (isset($_REQUEST['w']) and isset($_REQUEST['h'])) {
        $dirname="images_resized/";
        $width = $_REQUEST['w'];
        $height = $_REQUEST['h'];
    } else {
        $dirname="images/";
        $width = 0;
        $height = 0;
    }

    $images = array_diff(scandir($IMAGES_DIR),
                         array('..', '.', '.keep', 'old_images'));

    $images = array_values($images);

    $partial_json = "";
    foreach ($images as $image) {
        if ($width == 0) {
            $theimage = $image;
        } else {
            $theimage = explode('.', $image);
            $extension = $theimage[1];
            $theimage = $theimage[0];
            $theimage = "$theimage-$width-$height.$extension";
            resize_image($image, $theimage, $width, $height);
        }

        $partial_json .=  '{"type": "image",'
                        . '"url": "' . $dirname . $theimage . '", '
                        . '"html": "<img class=\"slide\" '
                        . 'src=\"' . $dirname . $theimage . '\"'
                        . 'alt=\"\">"}, ';
    }

    $partial_json = substr($partial_json, 0, -2);
    $num_images = count($images);

    return array($num_images, $partial_json);
}

function get_tweets() {
    // Não precisamos pegar de fato do twitter para testar
    /*
    $tmp = fopen("exemplo.json", "r") or die("Unable to open test file");
    $fake_response = fread($tmp, filesize("exemplo.json"));
    fclose($tmp);
    return json_decode($fake_response, true);
    */

    require 'config.php';
    $connection = new tmhOAuth(array(
        'consumer_key' => $twitter_consumer_key,
        'consumer_secret' => $twitter_consumer_secret,
        'user_token' => $twitter_user_token,
        'user_secret' => $twitter_user_secret,
    ));
    date_default_timezone_set($timezone);

    $parameters = array();
    $parameters['q'] = $twitter_hashtag
                       . ' AND -filter:retweets AND -filter:replies';

    $parameters['count'] = $twitter_hashtag_count;
    $parameters['result_type'] = 'recent'; // default is "mixed"
    $parameters['tweet_mode'] = 'extended';
    $parameters['include_entities'] = 'true';
    $url = 'https://api.twitter.com/1.1/search/tweets.json';

    $http_code = $connection->request('GET', $connection->url($url),
                                      $parameters);

    if ($http_code === 200) {
        $response = strip_tags($connection->response['response']);
        return json_decode($response, true);
    } else {
        echo "Error ID: ", $http_code, "<br>\n";
        echo "Error: ", $connection->response['error'], "<br>\n";
    }
}

function format_tweet(array $tweet) {
    $thetext = $tweet['full_text'];

    # This is the size if there are no URLs / qr codes
    $fontsize = strval(65/sqrt(strlen($thetext)));

    # Create QR Code for the first embedded link (if any)
    $theqrcode = "";
    foreach ($tweet['entities']['urls'] as $url) {
        // Smaller size if there is a URL / qr code
        $fontsize = strval(.9 * floatval($fontsize));

        $theqrcode =  '<span id=\"twitter_qrcode\">'
                    . '<img src=\"data:image/svg+xml;base64,'
                    .     generateqrcode($url['url']) . '\">'
                    . '</span>';

        $thetext = str_replace($url['url'], "", $thetext);

        break; // only the first one, if any exist
    }

    # Include the first embedded image in the page (if any)
    $theimg = "";
    if (array_key_exists('media', $tweet['entities'])) {
        foreach ($tweet['entities']['media'] as $img) {
            if ($img['type'] != "photo") continue;

            // Smaller size if there is an image
            $fontsize = strval(.9 * floatval($fontsize));

            $theimg =  '<div id=\"twitter_img\">'
                     .     '<img src=\"' . $img['media_url_https'] . '\">'
                     . '</div>';

            $thetext = str_replace($img['url'], "", $thetext);
            break; // only the first one, if any exist
        }
    }

    $html =  '<div id=\"twitter_bg\">'
           . '<div id=\"twitter_content\"; '
           .             'style=\"font-size: ' . $fontsize . 'vmin\" \">'
           .     '<div id=\"twitter_title\">'
           .         '<span id=\"username\">'
           .             '@' . $tweet['user']['screen_name']
           .         '</span>'
           .         '<img id=\"twitter_logo\" src=\"twitterlogo.png\"'
           .         'alt=\"\">'
           .     '</div>'
           .     '<div id=\"twitter_body\">'
           .         '<div id=\"twitter_text\">'
           .             str_replace('"', '\"', $thetext)
           .         '</div>'
           .         $theimg // may be empty
           .     '</div>'
           .     '<div id=\"twitter_footer\">'
           .         '<span id=\"twitter_timestamp\">'
           .             strftime('%d/%m/%Y - %H:%M',
                              strtotime($tweet['created_at']))
           .         '</span>'
           .         $theqrcode // may be empty
           .     '</div>'
           . '</div>'
           . '</div>';

    return $html;
}


function generateqrcode($url){
    //$fg = new Rgb(29, 161, 242); // blue
    $fg = new Rgb(0, 0, 0);
    $bg  = new Rgb(245, 248, 250);
    $fill = Fill::uniformColor($bg, $fg);
    $style = new RendererStyle(100, 0, null, null, $fill);

    $renderer = new ImageRenderer(
        $style,
        new SvgImageBackEnd()
    );

    // B&W rendering
    /*
    $renderer = new ImageRenderer(
        new RendererStyle(150),
        new SvgImageBackEnd()
    );
    */

    $writer = new Writer($renderer);
    return base64_encode($writer->writeString($url));
}

function tweets() {
    $num_tweets = 0;
    $partial_json = "";

    $tweets = get_tweets();

    if ($tweets != null) {
        foreach ($tweets['statuses'] as $tweet) {
            # Tweets older than 30 days are filtered out
            if(strtotime($tweet['created_at']) < strtotime('-30 days')) {
                continue;
            }

            $num_tweets += 1;

            $html = format_tweet($tweet);

            $partial_json .=  '{'
                            .     '"type": "tweet",'
                            .     '"html": "' . $html . '"'
                            . '}, ';
        }

        $partial_json = substr($partial_json, 0, -2);
        $partial_json = trim(preg_replace('/\s+/', ' ', $partial_json));
    }

    return array($num_tweets, $partial_json);
}

function videos() {
    global $VIDEOS_DIR;

    $video_list = array_diff(scandir($VIDEOS_DIR),
                             array('..', '.', '.keep', 'old_videos'));

    $num_videos = count($video_list);
    $partial_json = "";
    foreach ($video_list as $video) {
        $html =  '<video class=\"avideo\" muted>'
               . '<source src=\"' . $VIDEOS_DIR . '/'. $video . '\"></video>';
        
        $partial_json .=  '{'
                        .   '"type": "video",'
                        .   '"html": "' . $html . '"'
                        . '}, ';
    }
    $partial_json = substr($partial_json, 0, -2);
    return array($num_videos, $partial_json);
}

function pages() {
    global $PAGES_DIR;

    $page_list = array_diff(scandir($PAGES_DIR),
                            array('..', '.', '.keep', 'old_pages'));

    $num_pages = count($page_list);
    $partial_json = "";
    foreach ($page_list as $page) {
        $html =  '<iframe src=\"' . $PAGES_DIR . '/' . $page . '\" '
               . 'style=\"border:none\" '
               . 'width=\"100%\" height=\"100%\"></iframe>';

        $partial_json .=  '{'
                        .   '"type": "page",'
                        .   '"html": "' . $html . '"'
                        . '}, ';
    }

    $partial_json = substr($partial_json, 0, -2);
    return array($num_pages, $partial_json);
}

function generate_json(array $images, array $tweets,
                       array $videos, array $pages) {

    $json =  '{ "length": '
           .  strval($images[0] + $tweets[0] + $videos[0] + $pages[0])
           . ', ';

    $json .= '"num_images": ' . $images[0] . ', ';
    $json .= '"num_tweets": ' . $tweets[0] . ', ';
    $json .= '"num_videos": ' . $videos[0] . ', ';
    $json .= '"num_pages": ' . $pages[0] . ', ';
    $json .= '"documents": [ ';

    $json .= $images[1];
    if ($images[0] != 0
        and ($tweets[0] != 0
             or $videos[0] != 0
             or $pages[0] != 0)) {

        $json .= ', ';
    }

    $json .= $tweets[1];
    if ($tweets[0] != 0 and ($videos[0] != 0 or $pages[0] != 0)) {
        $json .= ', ';
    }

    $json .= $videos[1];
    if ($videos[0] != 0 and $pages[0] != 0){
        $json .= ', ';
    }

    $json .= $pages[1];

    $json .= " ] } ";

    $json = json_decode($json, true);
    shuffle($json['documents']);
    $json = json_encode($json);

    return $json;
}


echo generate_json(images(), tweets(), videos(), pages());

?>
