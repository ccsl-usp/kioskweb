# Kioskweb
A simple kiosk application running on the web.  
It display images, videos, web pages and the latest tweets from a specific user and a specific hashtag.

## Dependencies
tmhOAuth.php: https://github.com/themattharris/tmhOAuth  
cacert.pem: http://curl.haxx.se/ca/cacert.pem  
PHP Imagick extension

## Installation
Download the first and second dependencies to this directory.  
Then fill in config.php.template accordingly and rename it to config.php.

## Important
Tweets older than 30 days are filtered out.  
Videos and web pages must be filled in config.php as explained there.

## License
Copyright (C) 2014-2015 Leonardo Schäffer

Copyright (C) 2022 Leonardo Schäffer, Nelson Lago

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
