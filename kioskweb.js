// Authors: Leonardo Schaffer and Nelson Lago
// Distributed under the GPLv3 licence

// Approximate time for each image to be visible
var TIME = 8000;

// Reload the list of images after we show this number of images;
// 50 images corresponds to every 6-7 minutes
var RELOAD_EVERY = 50;

var json;

var reloading = false;

var changing = false;

var images_shown = 0;

// The very first iteration is different
var first_iteration_done = false

// When the display time for the current image ends, we want to show the
// next image or page "immediately", so we cannot start loading it then.
// What we do is, we load the next image or page on a different, hidden
// div, and alternate which div is shown in each iteration.
var this_div = '#first';
var that_div = '#second';
var document = 'index.html';
// TODO: now that we always load the next element to
//       be shown in advance, should we remove this?


function preload(items) {
    for (var item in items) {
        if (item.type === "image") {
            var img = new Image();
            img.validate = "never";
            img.src = item.url;
        };
    };
}

function changeDocumentDisplayed(i) {
    // Let's not run two instances of
    // this function at the same time.
    // No synchronization problem because
    // javascript is single-threaded
    if (changing) return;
    changing = true;

    // Throws if json is null or the wrong type or empty;
    // this should only happen before the first successful
    // execution of "reload".
    try {
        if (json.length === 0) throw "empty";
    } catch(e) {
        changing = false;
        setTimeout(reload, 0);
        setTimeout(changeDocumentDisplayed, 4000, 0);
        return;
    }

    if (! first_iteration_done) {
        $(this_div).css("display", "none");
        preloadDocInDiv(json.documents[0].html, this_div);
        first_iteration_done = true;
    }

    // Nothing was thrown so json is a valid object
    // of length > 0; we can move forward.

    // Let's reload the images every now and then.
    // Note that this does not affect the current cycle:
    //
    // 1. Before reload finishes, the cycle continues
    // 2. If reload fails, it tries again by itself
    // 3. After reload finishes, we continue navigating
    //    the array. If "json" shrinks to a size smaller
    //    than "i", there is an exception, which makes
    //    the cycle to restart from zero.
    // 4. Multiple concurrent calls to reload cause no harm

    images_shown += 1;
    if (images_shown >= RELOAD_EVERY) {
        images_shown = 0;
        setTimeout(reload, 0);
    }

    // Now let's actually (1) change the displayed image
    // and (2) schedule the next change. This should
    // not fail but, if it does, start over from zero.

    try {
        next = (i + 1) % json.length;

        // Swap divs for this iteration
        tmp = this_div;
        this_div = that_div;
        that_div = tmp;

        $(this_div).fadeOut(700, function() {
                                     $(that_div).fadeIn(700);

                                     preloadDocInDiv(
                                             json.documents[next].html,
                                             this_div);
                                 }
                           );

        if (json.documents[i].type == "video") {
            var video = document.querySelector(that_div + " .avideo");
            video.play();

            var video_event_listener = function (e) {
                    video.removeEventListener('ended', video_event_listener);
                    setTimeout(changeDocumentDisplayed, 0, next);
                    }

            video.addEventListener('ended', video_event_listener, false);
                        
        } else setTimeout(changeDocumentDisplayed, TIME, next);

        changing = false;

    } catch (e) {
        changing = false;
        setTimeout(changeDocumentDisplayed, 1000, 0);
    };
}

function preloadDocInDiv(doc, div) {
    if(doc.search('iframe') > 0) {
        // TODO this is ugly, there probably is a better way
        url_start = doc.search('"') +1;
        url_end = doc.indexOf('"', url_start);
        theurl = doc.substring(url_start, url_end);

        // Load the next iframe on the next div
        $.ajax(
            {
               url: theurl,
               success: function(result) { $(div).html(doc); },
               error: function() { setTimeout(reload, 0); }
            }
        );
    } else {
        // Load the next image or tweet in the next div
        $(div).html(doc);
    }
}

function reload() {
    // Let's not run two instances of
    // this function at the same time.
    //
    // No synchronization problem because
    // javascript is single-threaded
    if (reloading) return;
    reloading = true;

    var candidatejson;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {

        if (xmlhttp.readyState != 4)
            // Still downloading, just wait
            // for the next state change
            return;

        if (xmlhttp.status === 200) {
            // Request ended successfully
            try {
                candidatejson = xmlhttp.responseText.split('\n')[0];
                
                candidatejson = candidatejson.replace(/}[^{}]*$/, '}');
                
                candidatejson = JSON.parse(candidatejson);
                if (candidatejson.length === 0) throw "empty";

                // Force images to load into the browser cache
                preload(candidatejson.documents);
                console.log(candidatejson);
                // Nothing was thrown, so the received json
                // seems sane; make it available for the
                // rest of the script
                json = candidatejson;
                console.log(json);
                reloading = false;
            } catch (e) {
                // Something wrong with the data, try again
                reloading = false;
                setTimeout(reload, 1000);
            };
        }
        else {
            // The request failed, try again
            reloading = false;
            setTimeout(reload, 1000);
        };
    };

    // Do the request
    xmlhttp.open("GET", "kioskweb.php?w=" + window.innerWidth + "&h=" + window.innerHeight, true);
    xmlhttp.send();
}

function main() {
    setTimeout(changeDocumentDisplayed, 0, 0);
}
